#!/bin/bash

source ./test-lib.sh

if [ -z "$cvsps" ]; then
    echo "You need to build cvsps first !"
    exit 1
fi

breakages=0
known_breakages=0
fixed_bugs=0

find . -maxdepth 1 -type d -name "t[0-9]*" |
  sed -e "s/^\.\///" > /tmp/tests_to_run

while read i
do
    printf "%-40s" $i | tr -d "\n"
    run_cvsps $i
    result=$?
    if test -e $i/known_to_fail
    then
        known_break="yes"
    else
        known_break="no"
    fi
    case "$result,$known_break" in
        0,no)
        say_color "ok"
        ;;
        0,yes)
        say_color "fixed"
        fixed_bugs=$[$fixed_bugs+1]
        ;;
        1,no)
        say_color "broken"
        breakages=$[$breakages+1]
        ;;
        1,yes)
        say_color "known breakage"
        known_breakages=$[$known_breakages+1]
        ;;
        *)
        say_color "Error: something unknown went wrong..."
    esac
done < /tmp/tests_to_run

echo "Breakages: $breakages"
echo "Known Breakages: $known_breakages"
echo "Fixed Bugs: $fixed_bugs"
