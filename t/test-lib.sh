#!/bin/bash

# some sanity checks
type cvs >/dev/null || exit 1

export CVSROOT=$(pwd)/cvsroot

if [ -e ../cvsps ]; then
    cvsps=$(pwd)/../cvsps
fi

function say_color () {
    (
    case "$1" in
        broken) tput bold; tput setaf 1;; # bold red
        fixed)  tput bold; tput setaf 2;; # bold green
        ok)  tput setaf 2;;            # green
        known*)  tput setaf 3;;            # brown
        *) test -n "$quiet" && return;;
    esac
    printf "* %s" "$*"
    tput sgr0
    echo
    )
}

function run_cvsps {
    test_name=$1
    (
    cd $test_name
    $cvsps -x $test_name > actual 2> $test_name.log
    if diff -u actual expect >> $test_name.log  ; then
        return 0
    fi
    return 1
    )
}
